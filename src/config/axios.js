import axios from "axios";
import appConfig from "@/config/app";

let apiUrl =
  process.env.VUE_APP_STAGE === "LOCAL"
    ? process.env.VUE_APP_LOCAL_API_URL
    : process.env.VUE_APP_STAGE === "DEV"
    ? process.env.VUE_APP_DEV_API_URL
    : process.env.VUE_APP_PROD_API_URL;

const $axios = axios.create({
  baseURL: apiUrl,
  timeout: 10000
});

$axios.interceptors.request.use(
  config => {
    if (localStorage.getItem(appConfig.APP_VERSION)) {
      let storeData = JSON.parse(localStorage.getItem(appConfig.APP_VERSION));

      if (storeData && Object.keys(storeData).length) {
        let authData = storeData.auth;

        if (authData && Object.keys(authData).length) {
          let tokens = authData.authenticationDetails;

          config.headers.common["Authorization"] =
            "Bearer " + tokens.access_token;
        }
      }
    } else {
      config.headers.common["Authorization"] = "";
    }

    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

export default $axios;
