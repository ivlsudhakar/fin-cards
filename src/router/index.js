import Vue from "vue";
import Router from "vue-router";
import Home from '@/route-layouts/Home';
import Cards from '@/route-layouts/Cards'
import Payments from '@/route-layouts/Payments'
import Credit from '@/route-layouts/Credit'
import Settings from '@/route-layouts/Settings'
import DesktopDisplayCardsRoute from '@/route-layouts/DesktopDisplayCardsRoute'
import MobileDisplayCardsRoute from '@/route-layouts/MobileDisplayCardsRoute'
// import VueAwesomeSwiper from 'vue-awesome-swiper';
// import 'swiper/dist/css/swiper.css';



Vue.use(Router);
// Vue.use(VueAwesomeSwiper);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [    
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/cards',
      name: 'Cards',
      component: Cards
    },
    {
      path: '/payments',
      name: 'Payments',
      component: Payments
    },
    {
      path: '/credit',
      name: 'Credit',
      component: Credit
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings
    },
    {
      path: '/desktop',
      name: 'DesktopDisplayCardsRoute',
      component: DesktopDisplayCardsRoute
    },
    {
      path: '/mobile',
      name: 'MobileDisplayCardsRoute',
      component: MobileDisplayCardsRoute
    },

  ]
});
