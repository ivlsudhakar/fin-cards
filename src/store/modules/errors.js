// import axios from "@/config/axios";

// initial state
const initialState = () => {
  return {
    errors: [],
    message: null,
    error: null
  };
};
const state = initialState();

const getters = {
  getErrors: state => state.errors,
  getMessage: state => state.message,
  getError: state => state.error
};

const mutations = {
  setErrors: (state, errors) => {
    state.errors = errors;
  },
  setMessage: (state, message) => {
    state.message = message;
  },
  setError: (state, error) => {
    state.error = error;
  },
  clear: state => {
    state.errors = [];
    state.message = null;
    state.error = null;
  }
};

const actions = {
  clear: ({ commit }) => {
    commit("errors/clear", null, { root: true });
  },
  // error response
  errorResponse: (context, errorResponse) => {
    if (errorResponse && errorResponse.status) {
      let errorStatusCode = errorResponse.status;
      let errorData = [];
      switch (errorStatusCode) {
        case 422:
          if (errorResponse.data.data.errors) {
            errorResponse.data.data.errors.forEach(error => {
              errorData[error.key] = error.message;
            });
          }
          break;
        // case 401:
        //     break;
        case 403:
          if (errorResponse.data.message) {
            errorData["error"] = errorResponse.data.message;
          } else {
            errorData["error"] = errorResponse.data.data.message;
          }
          break;

        default:
          if (errorResponse.data.message) {
            errorData["error"] = errorResponse.data.message;
          } else {
            errorData["error"] = errorResponse.data.data.message;
          }
          break;
      }
      context.commit("errors/setErrors", errorData, { root: true });
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
