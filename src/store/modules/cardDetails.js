
// initial state
const initialState = () => {
  return {
    card: null,
    cardsList: [],
    status_display_card_number: 0
  };
};

const state = initialState();

const getters = {
  getCard: state => state.card,
  getCardsList: state => state.cardsList,
  getStatusDisplayCardNumber: state => state.status_display_card_number
};

const mutations = {
  setStatusDisplayCardNumber: (state, status) => {
    state.status_display_card_number = status;
  },
  setCardsList: (state, data) => {
    state.cardsList = data;
  },
  setCard: (state, statusData) => {
    state.card = statusData;
  },
  addCard: (state,data) => {
    state.cardsList.push(data);
  },
  cancelCard: (state,data) => {
    state.cardsList.push(data);
  },
  freezeCard: (state,data) => {
    state.cardsList.push(data);
  }

};

const actions = {
  switchStatusDisplayCardNumber: ({ commit, dispatch },status) => {
    try {
      dispatch("errors/clear", null, { root: true });
      commit("cardDetails/setStatusDisplayCardNumber", status, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  initializeCardList: ({ commit, dispatch }) => {
    try {
      dispatch("errors/clear", null, { root: true });
      let cardsList = [{
        id: 1,
        card_number: '1234123412341234',
        first_name: 'First Name',
        last_name: 'Last Name',
        display_name: 'First Name Last Name',
        cvv: '123',
        valid_thru: '02/28',
        valid_from: '02/22',
        date_of_birth: '28/04/1987',
        is_active: 1,
        provider: 'My Bank',
        bill_date: 'Today',
        last_paid: 'Last Month',
        due_date: "Tomorrow",
        eligible_for_emi: 1
      }];
      commit("cardDetails/setCardsList", cardsList, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  fetchCardsList: ({commit,dispatch}) => {
    try {
      dispatch("errors/clear", null, { root: true });
      let cardsList = [{
        id: 1,
        card_number: '1234123412341234',
        first_name: 'First Name',
        last_name: 'Last Name',
        display_name: 'First Name Last Name',
        cvv: '123',
        valid_thru: '02/28',
        valid_from: '02/22',
        date_of_birth: '28/04/1987',
        is_active: 1,
        provider: 'My Bank',
        bill_date: 'Today',
        last_paid: 'Last Month',
        due_date: "Tomorrow",
        eligible_for_emi: 1
      }];
      commit("cardDetails/setCardsList", cardsList, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  fetchCard: ({commit, dispatch}, id) => {
    try {
      dispatch("errors/clear", null, { root: true });
      let card = {}
      let index = state.cardsList.findIndex((card) => card.id == id);
      if(index >= 0) {
        card = state.cardsList[index]
      }
      commit("cardDetails/setCard", card, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  addCardToList: ({commit, dispatch}, cardDetails) => {
    try {
      dispatch("errors/clear", null, { root: true });
      commit("cardDetails/addCard", cardDetails, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  cancelCardFromList: ({ commit, dispatch }, cardDetails) => {
    try {
      dispatch("errors/clear", null, { root: true });
      commit("cardDetails/cancelCard", cardDetails, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  },
  freezeCard: ({commit,dispatch}, cardDetails) => {
    try {
      dispatch("errors/clear", null, { root: true });
      commit("cardDetails/freezeCard", cardDetails, {
        root: true
      });
    } catch (error) {
      dispatch("errors/errorResponse", error.response, { root: true });
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
