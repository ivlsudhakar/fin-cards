import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";
import config from "@/config/app";

// import axios from "axios";
Vue.use(Vuex);

import cardDetails from "@/store/modules/cardDetails";
import errors from "@/store/modules/errors";

const debug = process.env.NODE_ENV !== "production";

const store = new Vuex.Store({
    modules: {
        cardDetails,
        errors
    },
    strict: debug,
    plugins: [
        createPersistedState({
            key: config.APP_VERSION,
            paths: ['cardDetails']
        })
    ]
});

export default store;