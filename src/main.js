import Vue from 'vue';
import axios from 'axios';
import VueRouter from 'vue-router';
import ElementUI from 'element-ui';
import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
import 'element-ui/lib/theme-chalk/index.css';

import App from '@/App.vue';
import router from "@/router";
import store from "@/store";
Vue.use(ElementUI,{locale});
locale.use(lang);
Vue.use(axios);
Vue.use(VueRouter);
Vue.use(store);
Vue.config.productionTip = false;


new Vue({
  el:'#app',
  router,
  store,
  render: h => h(App),
}).$mount('#app')
